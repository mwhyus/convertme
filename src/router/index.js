import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Home from '../pages/Home';
import Options from '../pages/Options';
import CurrencyList from '../pages/CurrencyList'
import {ConversionContextProvider} from '../utils/context/ConversionContext'

const MainStack = createNativeStackNavigator()
const MainStackScreen = () => (
    <MainStack.Navigator initialRouteName="Home">
        <MainStack.Screen name='Home' component={Home} options={{headerShown: false}}/>
        <MainStack.Screen name='Options' component={Options} options={{headerShown: false}}/>
        <MainStack.Screen name='CurrencyList' component={CurrencyList} options={({ route }) => ({
            title: route.params && route.params.title,
        })} />

    </MainStack.Navigator>
)

export default () => (
    <NavigationContainer>
        <ConversionContextProvider>
            <MainStackScreen />
        </ConversionContextProvider>
    </NavigationContainer>
)