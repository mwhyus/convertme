import React, { createContext, useState, useEffect } from 'react'
import { Alert } from 'react-native'
import {api} from '../api/index'


export const ConversionContext = createContext() 

const DEFAULT_BASE_CURRRENCY = 'USD'
const DEFAULT_SECOND_CURRENCY = 'GBP'


export const ConversionContextProvider = ({ children }) => {
    const [baseCurrency, _setBaseCurrency] = useState(DEFAULT_BASE_CURRRENCY)
    const [secondCurrency, setSecondCurrency] = useState(DEFAULT_SECOND_CURRENCY)
    const [date, setDate] = useState()
    const [rates, setRates] = useState({})
    const [isLoading, setIsLoading] = useState(true)

    const setBaseCurrency = (currency) => {
        setIsLoading(true)
        return api(`/latest?base=${currency}`)
            .then((res) => {
                _setBaseCurrency(currency)
                setDate(res.date)
                setRates(res.rates)
                setIsLoading(false)
            })
            .catch((error) => {
                console.log(error);
                Alert.alert('Something went wrong.', error.message)
            })
    }

    const swapCurrencies = () => {
        setBaseCurrency(secondCurrency)
        setSecondCurrency(baseCurrency)
    }

    const contextValue ={
        baseCurrency,
        secondCurrency,
        date,
        rates,
        swapCurrencies,
        setBaseCurrency,
        setSecondCurrency,
        isLoading
    }

    useEffect(() => {
        setBaseCurrency(DEFAULT_BASE_CURRRENCY)
    }, [])

    return (
        <ConversionContext.Provider value={contextValue}>
            {children}
        </ConversionContext.Provider>
    )
}