export default {
    text: '#343434',
    border: '#e2e2e2',
    blue: '#506D84',
    white: '#f5f6fa',
    textLight: '#718093',
    offWhite: '#ecf0f1',
    silver: '#bdc3c7'
}