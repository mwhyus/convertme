import React from 'react'
import { StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'
import colors from '../utils/colors'

const ConversionInput = ({text, onPress, ...props}) => {
    const containerStyles = [styles.container]
    
    if(props.editable === false){
        containerStyles.push(styles.containerDisabled);
    }


    return (
        <View style={containerStyles}>
            <TouchableOpacity style={styles.button} onPress={onPress}>
                <Text style={styles.text}>{text}</Text>
            </TouchableOpacity>
            <TextInput style={styles.input} {...props} />
        </View>
    )
}

export default ConversionInput

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.white,
        marginVertical: 10,
        marginHorizontal: 20,
        borderRadius: 5,
        height: 50,
        flexDirection: 'row'
    },
    containerDisabled: {
        backgroundColor: colors.offWhite
    },
    button: {
        backgroundColor: colors.white,
        padding: 12,
        borderRightColor: colors.border,
        borderRightWidth: 1,
        borderTopLeftRadius: 12,
        borderBottomLeftRadius: 12
    },
    text: {
        fontSize: 18,
        color: colors.blue,
        fontWeight: 'bold'
    },
    input: {
        flex: 1,
        padding: 10,
        color: colors.textLight
    }
})
