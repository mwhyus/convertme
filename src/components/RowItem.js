import React from 'react'
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import colors from '../utils/colors'

const RowItem = ({title, onPress, rightIcon}) => {
    return (
        <View style={styles.container}>
            <TouchableOpacity onPress={onPress} style={styles.row}>
                <Text style={styles.title}>{title}</Text>
                {rightIcon}
            </TouchableOpacity>
        </View>
    )
}

export default RowItem

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 12,
        paddingVertical: 16,
        backgroundColor: colors.white,
        borderBottomWidth: 1,
        borderBottomColor: colors.silver,
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    title: {
        color: colors.text,
        fontSize: 16
    },
    check: {
        height:20,
        width: 20
    }
})
