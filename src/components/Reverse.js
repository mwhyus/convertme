import React from 'react'
import { StyleSheet, Text, TouchableOpacity, Image } from 'react-native'
import { ICSwitch } from '../assets'
import colors from '../utils/colors'

const Reverse = ({text, onPress}) => {
    return (
        <TouchableOpacity onPress={onPress} style={styles.container}>
            <Image source={ICSwitch} style={styles.switch} resizeMode='contain' />
            <Text style={styles.text}>{text}</Text>
        </TouchableOpacity>
    )
}

export default Reverse

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20,
        marginBottom: 20
    },
    switch: {
        height: 25,
        width: 25
    },
    text: {
        color: colors.white,
        fontSize: 16,
        marginLeft: 8
    }
})
