import ICLogo from './money-currency.png'
import ICSwitch from './switch.png'
import ICMenu from './menu.png'
import ICCheck from './check.png'

export {
    ICLogo,
    ICSwitch,
    ICMenu,
    ICCheck
}