import React, {useContext} from 'react' 
import { FlatList, View, StyleSheet, Image } from 'react-native'
import { ICCheck } from '../../assets'
import RowItem from '../../components/RowItem'
import currencies from '../../data/currencies.json'
import {ConversionContext} from '../../utils/context/ConversionContext'

export default ({navigation, route={}}) => {
    const params = route.params || {}
    const {
        setBaseCurrency, 
        setSecondCurrency, 
        baseCurrency, 
        secondCurrency
    } = useContext(ConversionContext)

return (
    <View style={styles.container}>
        <FlatList 
            data={currencies} 
            renderItem={({item}) => {
                // const selected = params.activeCurrency == item
                let selected = false
                if(params.isBaseCurrency && item == baseCurrency){
                    selected = true
                } else if (!params.isBaseCurrency && item == secondCurrency){
                    selected = true
                }
                return (
                    <RowItem 
                        title={item} 
                        onPress={() => {
                            if(params.isBaseCurrency){
                                setBaseCurrency(item)
                            } else {
                                setSecondCurrency(item)
                            }
                            navigation.pop()
                        }} 
                        selected
                        rightIcon={
                            selected && (
                                <View>
                                    <Image source={ICCheck} style={styles.check} />
                                </View>
                            )
                        }


                    />
                )
            }} 
            keyExtractor={(item) => item} 
            showsVerticalScrollIndicator={false}   
        />
    </View>
);
}

const styles = StyleSheet.create({
    container: {
        padding: 14
    },
    check: {
        height: 24,
        width: 24
    }
})