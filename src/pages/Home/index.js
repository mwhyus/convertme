import { format } from 'date-fns'
import React, { useContext, useState } from 'react'
import { ActivityIndicator, Dimensions, Image, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { ICLogo, ICMenu, ILBackground } from '../../assets'
import ConversionInput from '../../components/ConversionInput'
import Reverse from '../../components/Reverse'
import colors from '../../utils/colors'
import { ConversionContext } from '../../utils/context/ConversionContext'

const screen = Dimensions.get('window')


const Home = ({navigation}) => {
    const [value, setValue] = useState('1')

    const {
        baseCurrency, 
        secondCurrency, 
        swapCurrencies, 
        rates,
        isLoading
    } = useContext(ConversionContext)
    
    const date = new Date()
    const conversionRate = rates[secondCurrency]

    console.log(conversionRate);


    return (
                <View style={styles.container}>
                    <ScrollView>
                        <View style={styles.header}>
                            <View style={styles.logoStyle}>
                                <View style={styles.logoContainer}>
                                    <Image 
                                        source={ILBackground}
                                        style={styles.logoBackground}
                                        resizeMode='contain'
                                    />
                                    <Image 
                                        source={ICLogo} 
                                        style={styles.logo}
                                        resizeMode='contain'
                                    />
                                </View>
                                <Text style={styles.textHeader}>ConvertMe</Text>
                            </View>
                            <TouchableOpacity style={styles.options} onPress={() => navigation.navigate('Options')}>
                                <Image source={ICMenu} style={styles.menu} resizeMode='contain'/>
                            </TouchableOpacity>
                        </View>

                    <View style={styles.content}>
                    {isLoading ?(
                        <ActivityIndicator color={colors.white} size='large' />
                    ) : (
                        <>
                        <ConversionInput 
                            text={baseCurrency}
                            onPress={() => 
                                navigation.navigate('CurrencyList', {
                                    title: 'Base Currency', 
                                    isBaseCurrency: true
                                })} 
                            keyboardType= 'numeric'
                            onChangeText={text => setValue(text)}
                            value={value}
                        />
                        <ConversionInput 
                            text={secondCurrency} 
                            onPress={() => 
                                navigation.navigate('CurrencyList', {
                                    title: 'Second Currency', 
                                    isBaseCurrency: false
        
                                })} 
                            keyboardType= 'numeric'
                            onChangeText={text => console.log('text', text)}
                            value={
                                value && `${(parseFloat(value) * conversionRate).toFixed(3)}`
                            }
                            editable={false}
                        />
                        <Text style={styles.textFooter}>
                            {`1 ${baseCurrency} = ${conversionRate} ${secondCurrency} as of ${format(date, 'MMMM do, yyyy')}.`}
                        </Text>
        
                        <Reverse text='Switch Currencies' onPress={() => swapCurrencies()} />
                        </>
                    )}
              </View>
            </ScrollView>
        </View>
    )

}

export default Home

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.blue,
        flex: 1,
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    options: {
        marginHorizontal: 16,
        marginVertical: 16
    },
    content: {
        paddingTop: screen.height * 0.3
    },
    logoContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 16,
        marginRight: 8
    },
    logo: {
        position: 'absolute',
        height: screen.width * 0.08,
        width: screen.width * 0.08
    },
    logoStyle: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    logoBackground: {
        height: screen.width * 0.15,
        width: screen.width * 0.09,
    },
    textHeader: {
        color: colors.white,
        fontWeight: 'bold',
        fontSize: 20,
    },
    menu: {
        width: 26,
        height: 26
    },
    textFooter: {
        color: colors.white,
        fontSize: 13,
        textAlign: 'center',
    }
})
